Data = {
  products: [
    {
      id: 1,
      name: 'Suick Non-Weighted Thriller Minnow',
      price: '$16.99',
      image: '1.jpg',
      image_thumb: '1_thumb.jpg',
      description: 'The Suick Thriller series is known for its ability to pull fish out of the water. Made with wood, this minnow bait offers natural buoyancy with a non-weighted design that lets it swim over weeds and cover to prevent snags. The Suick Non-Weighted Thriller™ boasts the same diving and rising action as its bigger brother with the ability to be fished with lighter tackle to help you target smaller but equally difficult fish. '
    },
    {
      id: 2,
      name: 'Musky Mania Jake Crankbait',
      price: '$14.99',
      image: '2.jpg',
      image_thumb: '2_thumb.jpg',
      description: 'The Musky Mania Jake is a classic minnow for pulling out big fish. Its narrow, flat-sided body swims with a hug wobble to imitate a struggling baitfish. Reflective finishes produce an incredible amount of underwater flash to enhance appearance. Built with a high-impact lip, the Musky Mania Jake™ Crankbait can take a beating on structures, trees, and rocks to drive monster fish into action. '
    },
    {
      id: 3,
      name: 'Lucky Craft Pointer 78SP Jerk Bait',
      price: '$13.99',
      image: '3.jpg',
      image_thumb: '3_thumb.jpg',
      description: 'The Lucky Craft Pointer 78SP Jerk Bait uses special brass weights to lower the center of gravity, causing it to wobble and vibrate when it\'s still. The lure suspends at a depth of 4 to 5 feet and emits an intriguing bass-calling vibration. You to easily achieve the "walk the dog" action under the water with a short twitch of your rod. The lure is perfect for a variety of conditions and ideal for bass, walleye, or trout fishing.'
    },
    {
      id: 4,
      name: 'Lucky Craft Pointer 100SP Jerk Bait',
      price: '$13.99',
      image: '4.jpg',
      image_thumb: '4_thumb.jpg',
      description: 'With special brass weights to lower the center of gravity, the Lucky Craft Pointer 100SP Jerk Bait wobbles and vibrates when the retrieving motion is stopped. The Pointer suspends at a depth of 4 to 5 feet and emits an intriguing bass-calling vibration. It also enables you to easily achieve the "walk the dog" action under the water with a short twitch of your rod, and is perfect for bass, walleye, or trout fishing. '
    },
    {
      id: 5,
      name: 'Lucky Craft S.K.T. DR RT Rattle In Crankbait',
      price: '$15.99',
      image: '5.jpg',
      image_thumb: '5_thumb.jpg',
      description: 'The Lucky Craft ® S.K.T. DR RT Rattle In Crankbait is ideal for use in cold water and muddy conditions. It uses and internal weighting system for great balance, and is a versatile medium running crankbait. The front lip is wide and brings the bait quickly down to depth. It uses flat sides, a tight wobble and a lot of vibration to provide consistent movements. It can be used to fish for a variety of fish species. '
    },
    {
      id: 6,
      name: 'Storm Giant FlatStick Crankbait',
      price: '$14.99',
      image: '6.jpg',
      image_thumb: '6_thumb.jpg',
      description: 'Thanks to its big body design, the Storm Giant FlatStick carries itself like a much larger bait. This stick bait features flat sides that create a wide wobble and maximize flash with the help of the UV Bright finish. Equipped with a loud rattle and 3 VMC® Black Nickel hooks, the Storm® Giant FlatStick Crankbait matches the power of bigger baits and easily handles the demands of muskie fishing. '
    },
    {
      id: 7,
      name: 'LIVETARGET Golden Shiner Lipless Crankbait',
      price: '$12.99',
      image: '7.jpg',
      image_thumb: '7_thumb.jpg',
      description: 'The LIVETARGET ® Golden Shiner Lipless Crankbait is outfitted for performance inside and out. The lipless design gives the profile a more realistic appearance. The inside is equipped with a harmonic rattle that attracts fish from northern pike and walleye to striped bass and trout. A consistent performer, the Golden Shiner is perfectly balanced for retrieves and drop shots. '
    },
    {
      id: 8,
      name: 'LIVETARGET Walking Frog',
      price: '$12.99',
      image: '8.jpg',
      image_thumb: '8_thumb.jpg',
      description: 'Realistic coloring and like-like body details make the LIVETARGET ® Walking Frog an excellent choice for surface fishing. The weighted tail helps you to cast longer and makes the lure\'s movement even more realistic. Use in open water and around edges for the best results. '
    },
    {
      id: 9,
      name: 'Storm FlatStick Jointed Crankbait',
      price: '$9.99',
      image: '9.jpg',
      image_thumb: '9_thumb.jpg',
      description: 'The Storm FlatStick Jointed has extra power and attention that begs for more attention in the water. Its jointed tail swims with a wide roll and extra swimming action that gives it an incredible life-like appearance. Equipped with a loud, deep rattle, premium VMC® hooks, and realistic body designs, the Storm® FlatStick Jointed Crankbait is one of the most highly visible lures out there.'
    },
    {
      id: 10,
      name: 'Bagley Diving B2 Crankbait',
      price: '$8.99',
      image: '10.jpg',
      image_thumb: '10_thumb.jpg',
      description: 'The Bagley ® Diving B2 Crankbait is perfect when you need to dive quickly a school of suspending fish. The round lip is equipped with a lead ballast, giving this lure one of the best sinking speeds available. The balsa wood body balances the action by remaining light and buoyant. Perfect for fishing along deep shorelines and structures. '
    }
  ]
};