(function(){
        
    // GLOBAL PROPERTIES
    var screenHeight = Ti.Platform.displayCaps.platformHeight;
    var screenWidth = Ti.Platform.displayCaps.platformWidth;
    var windowTop = 0;
    var statusBarStyle = null;
    
    if(Ti.Platform.osname == 'iphone' && parseInt(Ti.Platform.version) >= 7){
        screenHeight -= 20;
        windowTop = 20;
    }
    if(Ti.Platform.osname == 'iphone'){
        statusBarStyle = Ti.UI.iPhone.StatusBar.TRANSLUCENT_BLACK;
    }
    
    // HOME
    
    // home window
    var win = Ti.UI.createWindow({
        backgroundColor: '#fff',
        top: windowTop,
        statusBarStyle: statusBarStyle,
        navBarHidden: true,
        height: Ti.UI.FILL,
        width: Ti.UI.FILL
    });
    
    var homeView = Ti.UI.createView({
        layout: 'vertical',
        width: Ti.UI.FILL
    });
    
    var homeImage = Ti.UI.createImageView({
        image: '/images/home.png',
        top: 40
    });
    
    homeView.add(homeImage);
    
    var homeButton = Ti.UI.createButton({
        title: 'Enter Store',
        width: '80%',
        height: 40,
        top: 20,
        color: '#fff',
        font: {
            fontSize: 16,
            fontWeight: 'bold'
        },
        backgroundGradient: {
            type: 'linear',
            startPoint: { x: 0, y: 0 },
            endPoint: { x: 0, y: '100%' },
            colors: [
                { color: '#444' },
                { color: '#111', offset: 0.15 }            
            ]
        },
        borderRadius: 8
    });
    homeButton.addEventListener('click', function(e){
        showCatalog();
    });
    
    homeView.add(homeButton);
    
    win.add(homeView);
    
    
    
    // CATALOG
    
    var catalogView = Ti.UI.createView({
        layout: 'vertical',
        width: screenWidth,
        height: screenHeight,
        left: screenWidth,
        backgroundColor: '#ccc',
        top: 0,
        zIndex: 1
    });
    
    function showCatalog(){
        catalogView.animate({
            left: 0,
            duration: 250
        }, function(){});
    }
    function hideCatalog(){
        catalogView.animate({
            left: screenWidth,
            duration: 250
        }, function(){});
    }
    
    // title
    var catalogTitle = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: 40,
        backgroundColor: '#333',
        top: 0
    });
    catalogTitle.add(Ti.UI.createLabel({
        text: 'Products',
        color: '#ddd',
        font: {
            fontSize: 16,
            fontWeight: 'bold'
        }
    }));
    
    // back button
    var btnCatalogBack = Ti.UI.createLabel({
        text: '< Back',
        color: '#ddd',
        font: {
            fontSize: 14,
            fontWeight: 'bold'
        },
        left: 10
    });
    btnCatalogBack.addEventListener('click', function(e){
        hideCatalog();
    });
    catalogTitle.add(btnCatalogBack);
    catalogView.add(catalogTitle);
    
    // scroll view
    var productsScrollView = Ti.UI.createScrollView({
        showVerticalScrollIndicator: true,
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        layout: 'vertical',
        scrollType: 'vertical'
    });
    
    Ti.include('/data/data.js');
    var products = Data.products;
    
    for(var i = 0; i < products.length; i++){
        var product = products[i];
        var productView = Ti.UI.createView({
            height: 60,
            width: Ti.UI.FILL,
            backgroundGradient: {
                type: 'linear',
                startPoint: { x: 0, y: 0 },
                endPoint: { x: 0, y: '100%' },
                colors: [
                    { color: '#fff' },
                    { color: '#ccc', offset: 0.15 }            
                ]
            },
            product: product
        });
        productView.addEventListener('click', function(e){
            showProduct(this.product);
        });
        
        // product image
        var productImage = Ti.UI.createImageView({
            image: '/images/products/' + product.image_thumb,
            height: 40,
            width: 40,
            left: 5,
            top: 10
        });
        productView.add(productImage);
        
        // product details
        var productDetailsView = Ti.UI.createView({
            layout: 'vertical',
            top: 5,
            left: 55,
            width: 250
        });
        
        // product name
        var productName = Ti.UI.createLabel({
            text: product.name,
            color: '#333',
            font: {
                fontSize: 14,
                fontWeight: 'bold'
            },
            left: 0,
            width: Ti.UI.SIZE,
            ellipsize: true
        });
        productDetailsView.add(productName);
        
        // product price
        var productPrice = Ti.UI.createLabel({
            text: product.price,
            color: '#333',
            font: {
                fontSize: 12,
                fontWeight: 'bold'
            },
            left: 0
        });
        productDetailsView.add(productPrice);
        
        productView.add(productDetailsView);
        
        productsScrollView.add(productView);
    };
    
    catalogView.add(productsScrollView);
    
    win.add(catalogView);
    
    // PRODUCT
    var currentProductView = Ti.UI.createView({
        layout: 'vertical',
        height: screenHeight,
        width: screenWidth,
        left: screenWidth,
        top: 0,
        backgroundColor: '#fff',
        zIndex: 2
    });
    
    function showProduct(product){
        currentProductImage.image = '/images/products/' + product.image;
        currentProductName.text = product.name;
        currentProductDescription.text = product.description;
        currentProductPrice.text = product.price;
        
        currentProductView.animate({
            left: 0,
            duration: 250
        }, function(){});
    }
    function hideProduct(){
        currentProductView.animate({
            left: screenWidth,
            duration: 250
        }, function(){});
    }
    
    // title
    var currentProductTitle = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: 40,
        backgroundColor: '#333',
        top: 0
    });
    currentProductTitle.add(Ti.UI.createLabel({
        text: 'Product Details',
        color: '#ddd',
        font: {
            fontSize: 16,
            fontWeight: 'bold'
        }
    }));
    
    // back button
    var btnProductBack = Ti.UI.createLabel({
        text: '< Back',
        color: '#ddd',
        font: {
            fontSize: 14,
            fontWeight: 'bold'
        },
        left: 10
    });
    btnProductBack.addEventListener('click', function(e){
        hideProduct();
    });
    currentProductTitle.add(btnProductBack);
    currentProductView.add(currentProductTitle);
    
    
    // product details
    var currentProductScrollView = Ti.UI.createScrollView({
        height: Ti.UI.FILL,
        width: '100%',
        scrollType: 'vertical',
        showVerticalScrollIndicator: true,
        layout: 'vertical',
        color: '#333'
    });
    
    // image
    var currentProductImage = Ti.UI.createImageView({
        image: '',
        height: 240,
        width: 240,
        top: 20
    });
    currentProductScrollView.add(currentProductImage);
    
    // title
    var currentProductName = Ti.UI.createLabel({
        text: '',
        font: {
            fontSize: '18',
            fontWeight: 'bold'
        },
        top: 10,
        width: 300,
        color: '#333',
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    currentProductScrollView.add(currentProductName);
    
    // description
    var currentProductDescription = Ti.UI.createLabel({
        text: '',
        font: {
            fontSize: '14'
        },
        width: 300,
        color: '#333',
        top: 20
    });
    currentProductScrollView.add(currentProductDescription);
    
    // price
    var currentProductPrice = Ti.UI.createLabel({
        text: '',
        font: {
            fontSize: '18',
            fontWeight: 'bold'
        },
        width: 300,
        top: 20,
        color: '#333',
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    currentProductScrollView.add(currentProductPrice);
    
    var buyButton = Ti.UI.createButton({
        title: 'Buy Now',
        width: '60%',
        height: 40,
        top: 20,
        color: '#fff',
        font: {
            fontSize: 16,
            fontWeight: 'bold'
        },
        backgroundGradient: {
            type: 'linear',
            startPoint: { x: 0, y: 0 },
            endPoint: { x: 0, y: '100%' },
            colors: [
                { color: '#444' },
                { color: '#111', offset: 0.15 }            
            ]
        },
        borderRadius: 8
    });
    buyButton.addEventListener('click', function(e){
        Ti.Media.createSound({
            url : '/audio/buy.mp3'
        }).play();
        alert('Ya\'ll added a item t\'yer cart!');
    });
    currentProductScrollView.add(buyButton);
    
    currentProductScrollView.add(Ti.UI.createLabel({ height: 10, width: Ti.UI.FILL }));
    
    currentProductView.add(currentProductScrollView);
    
    win.add(currentProductView);
    
    // open window
    win.open();

})();
